#!/usr/bin/env python3

# bobubuin
# BOM, BUy, BUild and INventory helper
#
# Copyright (C) 2019 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact: bobubuin at mrabb dot it

"""
    @package
    Handle component traits
"""

import re
import operator


class TraitGroup:
    """
    A Trait group contains a list of traits of a components (group).
    This module handles different input notation.
    It provides a way to test if a component matches aother component
    requirements

    Note:
    values lower than 1e-25 are rounded and thus, are not handled properly
    """

    class Trait:
        """ Holds a component single trait (value, unit)"""

        decimals_map = {
            "y": 24,
            "z": 21,
            "a": 18,
            "f": 15,
            "p": 12,
            "n": 9,
            "µ": 6,
            "u": 6,  # allow u for micro
            "m": 3,
            "K": 3,  # allow uppercase k for kilo
            "k": 3,
            "M": 6,
            "G": 9,
            "T": 12,
            "P": 15,
            "E": 18,
            "Z": 21,
            "Y": 24,
        }

        prefix_map = {
            "y": 1e-24,
            "z": 1e-21,
            "a": 1e-18,
            "f": 1e-15,
            "p": 1e-12,
            "n": 1e-9,
            "µ": 1e-6,
            "u": 1e-6,  # allow u for micro
            "m": 1e-3,
            "K": 1e3,  # allow uppercase k for kilo
            "k": 1e3,
            "M": 1e6,
            "G": 1e9,
            "T": 1e12,
            "P": 1e15,
            "E": 1e18,
            "Z": 1e21,
            "Y": 1e24,
        }

        prefix_str = "".join([*prefix_map])

        # prefix_map.keys() except metric prefixe non compatible
        prefix_list = [
            "y",  # 0
            "z",  # 1
            "a",  # 2
            "f",  # 3
            "p",  # 4
            "n",  # 5
            "µ",  # 6
            "m",  # 7
            "k",  # 8
            "M",  # 9
            "G",  # 10
            "T",  # 11
            "P",  # 12
            "E",  # 13
            "Z",  # 14
            "Y"   # 15
        ]

        class ParseError(Exception):
            """ Parse error, nothing parsed """

        def __init__(self, value, unit):
            self.value = value
            self.unit = unit

        def __repr__(self):
            return str(self)

        @classmethod
        def from_unit_list(cls, value, units, allow_unitless, idx):
            """
            Try to guess a Trait from a value as string and the possible units
            from a component category
            """
            if not value or value.isspace():
                return None, ""
            if not units:
                return None, ""
            unit = units[0]
            if allow_unitless:
                formats = {
                    # integer(.decimal)? (prefix|unit)?
                    r'^([0-9]*\.?[0-9]+)((%s)|(([%s])(%s)?))?$' %
                    (unit, cls.prefix_str, unit):
                        lambda group: (
                            group[0],
                            group[4],
                            group[2] or group[5]
                            ),
                    # (integer prefix decimal) | (integer unit decimal)
                    r'^([0-9]+)(([%s])|(%s))([0-9]+)' %
                    (cls.prefix_str, unit):
                        lambda group: (
                            group[0] + '.' + (group[4] or 0),
                            group[2],
                            group[3]
                            )
                }
            else:
                formats = {}
                for test_unit in units:
                    # integer(.decimal)? (prefix)? unit
                    formats[r'^([0-9]*\.?[0-9]+)([%s])?(%s)$' % (
                        cls.prefix_str,
                        test_unit
                    )] = lambda group: (
                        group[0],
                        group[1],
                        group[2]
                    )
                    # x unit (y)?
                    formats[r'^([0-9]+)(%s)([0-9]+)' % (
                        test_unit)] = lambda group: (
                            group[0] + '.' + (group[2] or 0),
                            None,
                            group[1]
                            )
            for fmt, parsed in formats.items():
                #print(fmt, parsed)
                matching = re.search(fmt, value)
                if matching:
                    new_value, prefix, unit = parsed(matching.groups())
                    new_value = float(new_value)
                    if prefix:
                        new_value *= cls.prefix_map[prefix]
                        new_value = round(
                            new_value, cls.decimals_map[prefix] + 3)
                    indices = [i for i, u in enumerate(units)
                               if unit == u and i >= idx]
                    if indices:
                        idx = indices[0]
                    return cls(new_value, units[idx]), units[idx]

            # Nothing found
            raise cls.ParseError({
                "message": "Can not parse '%s'" % value,
                "units": units,
                "current unit index": idx,
                "allow unitless": allow_unitless
                })

        def __str__(self):
            as_str = ""
            branch_found = False
            value = self.value
            p_index = None

            if value == float('inf'):
                as_str = 'inf'
                branch_found = True
            elif value == 0:
                as_str = "0"
                branch_found = True

            if not branch_found:
                p_index = self.prefix_list.index("k") - 1
                while value >= 1000:
                    branch_found = True
                    value /= 1000
                    p_index += 1
                    if p_index >= (len(self.prefix_list) - 1):
                        break

            if not branch_found:
                p_index = self.prefix_list.index("m") + 1
                while value < 1:
                    branch_found = True
                    value *= 1000
                    p_index -= 1
                    if p_index <= 0:
                        break

            value = round(value, 3)
            if value == int(value):
                value = int(value)
            as_str = str(value)
            if branch_found and p_index is not None:
                as_str += self.prefix_list[p_index]
            return as_str + self.unit

    class TooManyTraits(Exception):
        """ The component traits definition contains less traits """

    class CategoryMismatch(Exception):
        """ Comparing 2 components which does not belong to the same category
        """

    categories = {
        "C": {
            'Capacity': {'unit': 'F', 'cmp': '='},
            'Maximum voltage': {'unit': 'V', 'cmp': '<'},
            'Capacity tolerance': {'unit': '%', 'cmp': '>'},
        },
        "CP": {
            'Capacity': {'unit': 'F', 'cmp': '='},
            'Maximum voltage': {'unit': 'V', 'cmp': '<'},
            'Capacity tolerance': {'unit': '%', 'cmp': '>'},
        },
        "R": {
            'Resistance': {'unit': 'Ω', 'cmp': '='},
            'Maximum power': {'unit': 'W', 'cmp': '<'},
            'Maximum voltage': {'unit': 'V', 'cmp': '<'},
            'Resistance tolerance': {'unit': '%', 'cmp': '>'},
            'Temperature coefficient': {'unit': 'ppm', 'cmp': '>'},
        },
        "D": {
            'Vf': {'unit': 'V', 'cmp': '>'},
            'Vr': {'unit': 'V', 'cmp': '<'},
            'If': {'unit': 'A', 'cmp': '<'},
        },
        "L": {
            'Inductance': {'unit': 'H', 'cmp': '='},
            'Resistance': {'unit': 'Ω', 'cmp': '>'},
            'Saturation current': {'unit': 'A', 'cmp': '<'},
        },
        "LED": {
            'Wavelength': {'unit': 'm', 'cmp': '='},
            'Vf': {'unit': 'V', 'cmp': '>'},
            'Typical current': {'unit': 'A', 'cmp': '>'},
            'Luminous intensity': {'unit': 'cd', 'cmp': '<'},
            'Viewing angle': {'unit': '°', 'cmp': '>'},
        },
        "RV": {
            'Resistance': {'unit': 'Ω', 'cmp': '='},
            'Maximum power': {'unit': 'W', 'cmp': '<'},
            'Maximum current': {'unit': 'A', 'cmp': '<'},
            'Maximum voltage': {'unit': 'V', 'cmp': '<'},
            'Resistance tolerance': {'unit': '%', 'cmp': '>'},
            'Temperature coefficient': {'unit': 'ppm', 'cmp': '>'},
        }
    }

    comparison = {
        '=': {
            'required': operator.eq,
            'default': 0,
            'choose_factor': 0
        },
        '<': {
            'required': operator.le,
            'default': 0,
            'choose_factor': 1
        },
        '>': {
            'required': operator.ge,
            'default': float('Inf'),
            'choose_factor': -1
        }
    }

    def __init__(self, category):
        self.category = category
        self.defined = {}
        self.default = {k: TraitGroup.comparison[v['cmp']]["default"] for k, v
                        in TraitGroup.categories[self.category].items()}

    @classmethod
    def from_value_list(cls, values, category):
        """ Create a trait group from a list of string representing traits """
        if category in cls.categories.keys():
            cat = cls.categories[category]
            units = [x['unit'] for x in cat.values()]
            units_mapping = {x['unit']: c for c, x in cat.items()}
            if len(values) > len(cat):
                raise cls.TooManyTraits({
                    "message": "too many traits for this category",
                    "received traits": values,
                    "%s category traits" % category: cat
                })
            values.extend([""] * (len(units) - len(values)))

            group = TraitGroup(category)
            for index, value in enumerate(values):
                trait, unit = cls.Trait.from_unit_list(
                    value,
                    units,
                    index == 0,
                    index)
                if trait:
                    # u != trait.unit is possible if sparse values are received
                    u_mapped = units_mapping[unit]
                    group.defined[u_mapped] = trait
                    group.default[u_mapped] = trait.value
            return group
        return None

    def fits(self, other):
        """ returns true if self may be safely replaced by other """
        if self.category != other.category:
            raise self.CategoryMismatch({
                "message": "Can not compare these traits",
                "self": self.category,
                "other": other.category
            })
        for cat, i, j in zip(
                [x['cmp'] for x in
                    TraitGroup.categories[self.category].values()],
                self.default.values(),
                other.default.values()
                ):
            if not TraitGroup.comparison[cat]["required"](i, j):
                return False
        return True

    def __str__(self):
        return "_".join([str(x) for x in self.defined.values()])


if __name__ == '__main__':

    import unittest

    class TestTraits(unittest.TestCase):
        """ Trait unit test """

        def test_all(self):
            """ Trait unit test """
            # Parsing
            # 1st value, 1st form
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1"], "R")), "1Ω")
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1.2"], "R")), "1.2Ω")
            self.assertEqual(str(TraitGroup.from_value_list(
                ["0.1Ω"], "R")), "100mΩ")
            self.assertEqual(str(TraitGroup.from_value_list(
                ["0.1K"], "R")), "100Ω")
            # 1st value, 2nd form
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1Y2"], "R")), "1.2YΩ")
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1Ω2"], "R")), "1.2Ω")

            # Next values, 1st form
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1", "1W"], "R")), "1Ω_1W")
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1", "1.4KW"], "R")), "1Ω_1.4kW")
            # Next values, 2nd form
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1", "1W5"], "R")), "1Ω_1.5W")

            # Sparse values
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1K2", "1.5mW", "1%5"], "R")), "1.2kΩ_1.5mW_1.5%")

            # Limits
            self.assertEqual(str(TraitGroup.from_value_list(
                ["0.1yΩ"], "R")), "0.1yΩ")
            self.assertEqual(str(TraitGroup.from_value_list(
                ["1200Y"], "R")), "1200YΩ")

            # Error, too much traits provided for this category
            with self.assertRaises(TraitGroup.TooManyTraits):
                TraitGroup.from_value_list(
                    ["1Ω", "2W", "100V", "1%", "50ppm", "0.1H"], "R")

            # Error, the unit is not recognized for this category
            with self.assertRaises(TraitGroup.Trait.ParseError):
                TraitGroup.from_value_list(["1Ω", "2F"], "R")
            # Error, a unit is required for non-first traits
            with self.assertRaises(TraitGroup.Trait.ParseError):
                TraitGroup.from_value_list(["1Ω", "2"], "R")

            # Comparison
            g_1 = TraitGroup.from_value_list(["1Ω"], "R")
            g_1w = TraitGroup.from_value_list(["1Ω", "1W"], "R")
            g_2w = TraitGroup.from_value_list(["1Ω", "2W"], "R")
            g_2 = TraitGroup.from_value_list(["1Ω", "2W", "1%"], "R")
            g_acc = TraitGroup.from_value_list(
                ["1Ω", "1W", "10V", "10%", "1000ppm"], "R")
            g_rej = TraitGroup.from_value_list(
                ["1Ω", "10W", "100V", "1%", "100ppm"], "R")
            g_c = TraitGroup.from_value_list(
                ["1"], "C")
            self.assertTrue(g_1.fits(g_1))
            self.assertTrue(g_1w.fits(g_2w))
            self.assertFalse(g_2w.fits(g_1w))
            self.assertFalse(g_2.fits(g_2w))
            self.assertTrue(g_acc.fits(g_rej))
            self.assertFalse(g_rej.fits(g_acc))
            self.assertTrue(
                TraitGroup.from_value_list(
                    ["1.2MΩ", "1%"], "R").fits(
                    TraitGroup.from_value_list(
                        ["1.2MΩ", "100mW", "50V", "1%", "100ppm"],
                        "R")))

            # Error, comparing different component categories
            with self.assertRaises(TraitGroup.CategoryMismatch):
                g_1.fits(g_c)

    unittest.main()
