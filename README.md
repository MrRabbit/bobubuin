# Bobubuin

BOM, BUy, BUild and INventory helper

Project status: draft

kicad_netlist_reader.py is provided by Kicad. The other files are under the AGPL licence.

## Purpose

bobubuin provides several tools:

* Per project BOM: bobukuin.py is a BOM generator script to be used with Kicad.
    * bobubuin tries to find what you have in your inventory so you don't have to buy new components for each project
    * bobubuin analyzes the component specification. If your project needs a 1µF/10V capacitor and you have some 1µF/6.3V and 1µF/16V capacitors, bobubuin will choose the 16V variant.
    * bobubuin produces a per-vendor order list of the missing components. (Not implemented yet)
* Keep your inventory up-to-date. A file describes your inventory, bobubuin updates it :
    * Tell bobubuin "I"m assembling 10 of these boards" and bobubuin substract the used components from the inventory. (Not implemented yet)
    * When you receive components, bobubuin updates the inventory according the specified order (through an API call) or by scanning received parts bag codes. (Not implemented yet)
* Use the GUI to see what you have in your inventory (Not implemented yet) or use the text-based inventory file with your own softwares

## How to use it

* build a inventory.json file (json format is to be specified).
* Set the DISTRIBUTOR_API_KEY_* keys as environment variables
* Run bobukuin.py through Kicad so it construct the .xml file too.
    * Eeschema: Tools -> generate Bill of Materials -> add the script
    * Command line example: python "<path/to/>bobukuin.py" -x "%I" -b "%O" <path/to/>inventory.json
    * 10 is the number of boards, '-b' means to write the BOM file

## TODOs

* [ ] Traits
    * [ ] allow the "X% tolerance" trait comparison
    * [ ] allow multiple symbols for trait units (eg. Ω, ohm, o, etc.)
    * [ ] allow multiple passive components in a single casing (e.g. diode x2 or resistor network)
    * [ ] allow space between value and unit (this should be the default format)
    * [ ] protection diodes have the 'D' letter but regular diodes properties do not fit in this case
* [ ] BOM computing
    * [ ] skip on error instead of exception
    * [ ] handle grouped sell (ie buy a bag of 40 units instead of having a price for 1 unit)
    * [ ] a component may use N in stock (ie. breakable pin header)
    * [ ] allow 'alias of' value' in .json (eg. avr isp for conn 2x3)
* [ ] output writer
    * [ ] Global csv list
    * [ ] export per distributor BOM list and a list of missing components
    * [ ] Call APIs to fill the basket
* [ ] Use choice list to choose which component to use
    * [ ] specify project minimum requirements (eg. Vcc = 3.3V)
    * [ ] selectors such as (les expensives, in stock)
* [ ] retrieve SKU from distributors from values (automatically fill the inventory.json)
    * [ ] allow declaring a range of components with the same datasheet in the .json
* [ ] create a tool to modify the stock
    * [ ] GUI table of components + 'add this part to list' + easy 'set stock' + TODO
    * [ ] modify stock quantities from a BOM (remove 'BOM.qnty' from the stock)
    * [ ] add components by flashing code
* [ ] GUI highlight components being placed for manufacturing
* [ ] use the manufacturer datasheet instead of the distributor's one
* [ ] give hint on which package and type (C or CP for instance) to give when designing

## needs triage

Findings :
- diodes : pas d'affichage de Vf dans la valeur
- bug gestion quantité de composants par paquet

Améliorations :
- quantité nécessaire de composants + 10 %
- sélection fournisseur préféré (si un composant équivalent est dispo chez le fournisseur préféré, sélection de celui-ci)
- ajouter une option pour ne mettre à jour que les composants qui n'ont pas de prix
- Régler le problème 403 farnell et faire tourner le script les nuits sur un serveur
