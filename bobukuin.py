#!/usr/bin/env python3

# bobubuin
# BOM, BUy, BUild and INventory helper
#
# Copyright (C) 2019 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact bobubuin at mrabb dot it

"""
    @package
    Generate a csv BOM and a shopping list from Kicad and your inventory
"""

# std imports
import argparse
import json
from shutil import copyfile
import kicad_netlist_reader
from _version import __version__
from component import BOM, Inventory
from global_resources import Global
from output import Output

parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument(
    '-x', '--net_xml',
    help='kicad xml generated netlist. This will create a csv BOM file')
parser.add_argument(
    '-b', "--bom",
    help='output BOM file',
    nargs='?',
    default='out.csv')
parser.add_argument(
    '-f', '--inventory_file',
    help='json inventory file',
    default=Global.inventory_filename,
    nargs='?')
parser.add_argument(
    '-c', '--config',
    help='Application config file path')
parser.add_argument(
    '-n', '--board_count',
    help='number of board to use for the order form',
    type=int,
    default=10,
    nargs='?')
parser.add_argument(
    '-u', '--update_distributors',
    help='retrieve the inventory info from the distributors',
    action='store_true')
parser.add_argument(
    '-s', '--save_inventory',
    help='save the updated inventory (to be used with -u)',
    action='store_true')
parser.add_argument(
    '-v', '--version',
    action='version',
    version='%(prog)s {version}'.format(version=__version__))
parser.add_argument(
    '-d', "--debug",
    help='debug mode, verbose output',
    action='store_true')
args = parser.parse_args()

# Set global definitions
Global.dbg = args.debug

if args.config:
    Global.config_file = args.config
Global.readConf()

# Always load the inventory
with open(args.inventory_file) as ivt:
    inventory_load = json.load(ivt)
inventory = Inventory(inventory_load)

# Update distributors information is requested
if args.update_distributors:
    inventory.update_distributors()

# Parse the netlist if requested
if args.net_xml:
    net = kicad_netlist_reader.netlist(args.net_xml)
    groups = net.groupComponents()
    bom = BOM(groups)
    bom.compute_all(inventory, args.board_count)
    out = Output(bom, args.board_count)
    out.write_csv(args.bom)

# Save the inventory and a backup if requested
if args.save_inventory:
    copyfile(
        args.inventory_file,
        args.inventory_file + ".bak")
    with open(args.inventory_file + '.new', 'w') as ivt:
        json.dump(inventory.to_object(), ivt, indent=4,
                  ensure_ascii=False)
