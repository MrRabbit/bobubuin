EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5D63A05E
P 2300 3000
F 0 "R1" H 2370 3046 50  0000 L CNN
F 1 "100K" H 2370 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2230 3000 50  0001 C CNN
F 3 "~" H 2300 3000 50  0001 C CNN
	1    2300 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D63A179
P 2700 3000
F 0 "R2" H 2770 3046 50  0000 L CNN
F 1 "0.1M" H 2770 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2630 3000 50  0001 C CNN
F 3 "~" H 2700 3000 50  0001 C CNN
	1    2700 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5D63A37F
P 3100 3000
F 0 "R3" H 3170 3046 50  0000 L CNN
F 1 "0M1" H 3170 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3030 3000 50  0001 C CNN
F 3 "~" H 3100 3000 50  0001 C CNN
	1    3100 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5D63A987
P 2300 3450
F 0 "R4" H 2370 3496 50  0000 L CNN
F 1 "100K_50V_0.001K%" H 2370 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2230 3450 50  0001 C CNN
F 3 "~" H 2300 3450 50  0001 C CNN
	1    2300 3450
	1    0    0    -1  
$EndComp
Text Notes 3550 3150 0    50   ~ 0
R1, 2 and 3 are grouped\nR4 and R5 are grouped\nR6 and R7 are grouped\nR9 produces an error (secondary parameter is unitless)
$Comp
L Device:R R6
U 1 1 5D63C103
P 2300 3900
F 0 "R6" H 2370 3946 50  0000 L CNN
F 1 "100Ω" H 2370 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2230 3900 50  0001 C CNN
F 3 "~" H 2300 3900 50  0001 C CNN
	1    2300 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5D63C71A
P 2700 3900
F 0 "R7" H 2770 3946 50  0000 L CNN
F 1 "100" H 2770 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2630 3900 50  0001 C CNN
F 3 "~" H 2700 3900 50  0001 C CNN
	1    2700 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5D63D8A9
P 3200 3450
F 0 "R5" H 3270 3496 50  0000 L CNN
F 1 "100K_0.05KV_1%" H 3270 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3130 3450 50  0001 C CNN
F 3 "~" H 3200 3450 50  0001 C CNN
	1    3200 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
