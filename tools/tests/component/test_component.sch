EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328PB-AU U101
U 1 1 5DCC28AF
P 3350 3500
F 0 "U101" H 3350 1911 50  0000 C CNN
F 1 "ATmega328PB-AU" H 3350 1820 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 3350 3500 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001906C.pdf" H 3350 3500 50  0001 C CNN
	1    3350 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R101
U 1 1 5DCC3296
P 6150 3050
F 0 "R101" H 6220 3096 50  0000 L CNN
F 1 "10K" H 6220 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 3050 50  0001 C CNN
F 3 "~" H 6150 3050 50  0001 C CNN
	1    6150 3050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
