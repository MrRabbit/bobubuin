#!/usr/bin/env python3

# bobubuin
# BOM, BUy, BUild and INventory helper
#
# Copyright (C) 2019 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact bobubuin at mrabb dot it

""" Retrieve distributor information on a component """

from global_resources import Global
import math
import requests
import sys
import time
import xml.etree.ElementTree as ET


class DistributorItemGroup:
    """ Defines all distributor sources of a component """

    def __init__(self, source=None):
        if source:
            self.dists = {k: DistributorItem(v) for k, v in source.items()}
        else:
            self.dists = {}

    def __str__(self):
        if self.dists:
            return ";".join(
                ["%s: %s;" % (k, v) for k, v in self.dists.items()])
        return "no distributors"

    def __repr__(self):
        return str(self)

    def get(self, dist):
        """ Get a dostributor by its name or None if it does not exist """
        return self.dists.get(dist)

    def to_object(self):
        """ Return the class fields to be saved """
        return {k: v.to_object() for k, v in self.dists.items()}


class DistributorItem:
    """ Defines a component sold by a distributor """

    def __init__(self, source):
        self.sku = source["sku"]
        self.prices = source["prices"]
        self.status = source.get("status") or ""
        self.quantity = source.get("quantity") or 1

    def __str__(self):
        return "%s, %s, %s, %s" % (
            self.sku,
            self.quantity,
            self.status,
            self.prices)

    def __repr__(self):
        return str(self)

    def to_object(self):
        """ Return the class fields to be saved """
        return {
            "sku": self.sku,
            "prices": self.prices,
            "status": self.status,
            "quantity": self.quantity
        }

    def getPrice(self, qnty):
        """ Get the quantity and unit price of components to order for the
        given required quantity """
        if not self.prices:
            return (float('inf'), 0)
        if qnty < 0:
            qnty = 0
        price_range = 0
        for i in self.prices[1:]:
            if i[0] > qnty:
                break
            price_range += 1
        pack_qnty = self.prices[price_range][0]
        unit_price = self.prices[price_range][1]
        packs_count = math.ceil(qnty / pack_qnty)
        return (packs_count * pack_qnty, unit_price)


class Distributor:
    """ A component distributor """

    def __init__(self, name, retrieve_price):

        # Set trivial members
        self.name = name
        self.retrieve_price = retrieve_price

        # Load API keys from a config file
        section = "distributor_api_keys"

        # Check section
        Global.readConf()
        if section not in Global.conf:
            Global.warn("No config file set yet")
            Global.conf[section] = {}

        # Retrieve key
        key_name = "distributor_api_key_%s" % name.lower()
        if key_name in Global.conf[section]:
            self.key = Global.conf[section][key_name]
        else:
            Global.warn("No key found for %s" % key_name)
            Global.conf[section][key_name] = "fill_me"
            Global.saveConf()

    def get_prices(self, items):
        """ Retrieve the price of a list of components from the distributor """
        try:
            self.retrieve_price(items, self.key)
        except AttributeError:
            Global.warn("Keys are used not defined")
            sys.exit(1)

    def __str__(self):
        return "%s" % (self.name)

    def __repr__(self):
        return self.__str__()


def farnell_retrieve(items, key):
    for i in items:
        Global.debug("[farnell] getting price of ", i)
        req = """
            https://api.element14.com/catalog/products?term=
            id:%s
            &storeInfo.id=fr.farnell.com
            &resultsSettings.offset=0
            &resultsSettings.numberOfResults=1
            &resultsSettings.refinements.filters=rohsCompliant,CinStock
            &resultsSettings.responseGroup=medium
            &callInfo.omitXmlSchema=false
            &callInfo.responseDataFormat=json
            &callinfo.apiKey=%s
            """ % (i.sku, key)
        req = req.replace(' ', '').replace('\n', '')
        hdr = {'content-type': 'application/json'}
        result = requests.get(req, headers=hdr)
        if result.status_code >= 300:
            Global.warn("error while requesting farnell: %d" %
                        result.status_code)
            return
        j = result.json()
        if j["premierFarnellPartNumberReturn"]["numberOfResults"] > 0:
            product = j["premierFarnellPartNumberReturn"]["products"][0]
            i.quantity = product["packSize"]
            i.status = product["productStatus"]
            i.prices = [(p["from"], p["cost"]) for p in product["prices"]]
        else:
            Global.warn("warning: %s not found (farnell)" % i.sku)
        time.sleep(2)


def mouser_retrieve(items, key):
    prefix = "{http://api.mouser.com/service}"

    for i in items:
        Global.debug("[mouser] getting price of ", i)
        url = "http://api.mouser.com/service/searchapi.asmx"
        headers = {'content-type': 'application/soap+xml; charset=utf-8'}
        body = """<?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:xsi=
            "http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
            <soap12:Header>
                <MouserHeader xmlns="http://api.mouser.com/service">
                <AccountInfo>
                    <PartnerID>%s</PartnerID>
                </AccountInfo>
                </MouserHeader>
            </soap12:Header>
            <soap12:Body>
                <SearchByPartNumber xmlns="http://api.mouser.com/service">
                <mouserPartNumber>"%s"</mouserPartNumber>
                <partSearchOptions>"Exact"</partSearchOptions>
                </SearchByPartNumber>
            </soap12:Body>
            </soap12:Envelope>""" % (key, i.sku)
        result = requests.post(url, data=body, headers=headers)
        if result.status_code >= 300:
            Global.warn("error while requesting mouser: %d" %
                        result.status_code)
            return
        r = result.content
        root = ET.fromstring(r)
        try:
            component = root[0][0][0][1][0]
            i.prices = [(
                int(p[0].text),
                float(p[1].text.replace(',', '.').replace('€', ''))) for p in (
                    component.find(prefix + 'PriceBreaks'))]
            i.status = component.find(prefix + 'Availability').text
            i.quantity = 1  # TODO retrieve quantity per pack if != 0
        except KeyError:
            Global.warn("warning: %s failed (mouser)" % i.sku)
        time.sleep(2)  # 30 calls / minute


def radiospares_retrieve(items, key):
    pass


class Distributors:
    """ Manage all known distributors """

    def __init__(self):
        self.dists = {
            "farnell": Distributor("farnell", farnell_retrieve),
            "mouser": Distributor("mouser", mouser_retrieve),
            "radiospares": Distributor("radiospares", radiospares_retrieve),
        }

    def distributors(self):
        """ Get the list of known distributor names """
        return list(self.dists.keys())

    def update_distributors(self, components):
        """ retrieve all prices of the given components in-place """
        items = {dist: [c.distributors.get(dist) for c in components if
                        c.distributors.get(dist)] for dist in self.dists}
        for dist, items in items.items():
            self.dists[dist].get_prices(items)
