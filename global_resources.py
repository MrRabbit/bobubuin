#!/usr/bin/env python3

# bobubuin
# BOM, BUy, BUild and INventory helper
#
# Copyright (C) 2019 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact bobubuin at mrabb dot it

""" Global resource files definition """

import configparser
import os
import pathlib
import sys


class Global:
    """ global resource files definition """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    bobukuin_etc = "BOBUKUIN_ETC"
    inventory_filename = "inventory.json"
    dbg = False
    config_file = os.path.join(
        pathlib.Path(__file__).parent.absolute(),
        "bobubuin.conf")
    conf = configparser.ConfigParser()

    # Inventory path search order:
    # 1 command line
    # 2 env var
    # 3 ./components.json
    if bobukuin_etc in os.environ:
        inventory_filename = os.path.join(os.environ[bobukuin_etc],
                                          inventory_filename)
    else:
        inventory_filename = os.path.join(dir_path, inventory_filename)

    def __init__(self):
        pass

    def __str__(self):
        return "not to be be instantiated"

    def warn(*args):
        print(*args, file=sys.stderr)

    def debug(*args):
        if Global.dbg:
            print(*args)

    def readConf():
        Global.conf.read(Global.config_file)

    def saveConf():
        with open(Global.config_file, 'w+') as configfile:
            Global.conf.write(configfile)
