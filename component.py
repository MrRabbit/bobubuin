#!/usr/bin/env python3

# bobubuin
# BOM, BUy, BUild and INventory helper
#
# Copyright (C) 2019 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact bobubuin at mrabb dot it

""" BOM and inventory components """

from trait import TraitGroup
from global_resources import Global
import distributors
import json


def json_serial(obj):
    if isinstance(obj, (ComponentInventory, ComponentBOM)):
        return obj.__repr__()
    raise TypeError("Type %s not serializable" % type(obj))


class Component:
    """Base component"""

    def __init__(self, value, category, footprint, quantity):
        self.category = category
        self.footprint = footprint
        self.quantity = quantity
        self.traits = TraitGroup.from_value_list(value.split('_'), category)
        if self.traits:
            self.value = str(self.traits)
        else:
            self.value = value

    def __eq__(self, other):
        fields = ['value', 'category', 'footprint']
        for field in fields:
            if self.__dict__[field] != other.__dict__[field]:
                return False
        return True

    def __str__(self):
        return "<%s, %s, %s, qnty=%d>" % (
            self.category,
            self.footprint,
            self.value,
            self.quantity)

    def __repr__(self):
        return str(self)

    def fits(self, other):
        """ computes whether other may be used instead of self based on the
        traits """
        if self.traits:
            return self.traits.fits(other.traits)
        return self.value == other.value


class ComponentBOM(Component):
    """Component with BOM specific attirbuts"""

    def __init__(self,
                 value,
                 category,
                 footprint,
                 quantity,
                 refs,
                 inventory=None):
        self.refs = refs
        self.inventory = inventory
        self.to_order = 0
        # Try with an alternate category
        self.subcategory = category
        if category not in TraitGroup.categories.keys():
            category = self.compute_ref_category()
        super().__init__(value, category, footprint, quantity)

    def __str__(self):
        return Component.__str__(self) + "[%s, %s, order=%d] %s" % (
            "-".join(self.refs),
            self.subcategory,
            self.to_order,
            self.inventory.str_own_parameters()
            if self.inventory else "[not found]"
            )

    def __repr__(self):
        return str(self)

    def compute_ref_category(self):
        """ Retrieve the category prefix from the references """
        if self.refs:
            return ''.join(i for i in self.refs[0] if i.isalpha()).upper()
        return ''


class BOM:
    """ BOM list of component groups """

    def __init__(self, kicad_group):
        self.components = []
        for group in kicad_group:
            refs = []
            for component_current in group:
                refs.append(component_current.getRef())
                component = component_current
            value = component.getValue()
            category = component.getPartName()
            footprint = component.getFootprint()
            quantity = len(group)
            self.components.append(ComponentBOM(
                value,
                category,
                footprint,
                quantity,
                refs
            ))
        Global.debug("BOM", str(self))

    def __str__(self):
        ret = ""
        for i in self.components:
            ret += str(i) + "\n"
        return ret

    def __repr__(self):
        return str(self)

    def compute_all(self, inventory, board_count):
        """
        Compute the component list to pick from the inventory and their
        ordering information
        """
        for i in self.components:
            i.inventory = inventory.find_best(i)
            if i.inventory:
                i.value = i.inventory.value
        self.merge()
        self.sort()
        self.compute_order_amount(board_count)

    def merge(self):
        """ Merge component groups refering to the same component """
        for i, _ in enumerate(self.components):
            j = i + 1
            while j < len(self.components):
                same_group = False
                if (
                        (self.components[i].inventory) and
                        (self.components[j].inventory) and
                        self.components[i].inventory ==
                        self.components[j].inventory):
                    same_group = True
                elif self.components[i] == self.components[j]:
                    same_group = True
                if same_group:
                    self.components[i].refs.extend(self.components[j].refs)
                    self.components[i].quantity += self.components[j].quantity
                    del self.components[j]
                else:
                    j += 1

    def sort(self):
        """ Sort component by category """
        def category_priority(category_name):
            cat = category_name
            priority = ['C', 'R', 'D', 'L', 'U', 'J']
            # If in predefined list use this order
            if cat in priority:
                return priority.index(cat)
            # else, fiddle a string to int sorting value
            rank = len(priority)
            while cat:
                rank += ord(cat[0])
                rank *= 256
                cat = cat[1:]
            return rank

        self.components.sort(key=lambda x: (
            category_priority(x.category),
            x.footprint,
            x.value
        ))

    def compute_order_amount(self, board_count):
        """
        Compute how many components to order based on the target board count
        and the invenotry
        """
        for i in self.components:
            if i.inventory:
                i.to_order = max(
                    0,
                    i.quantity * board_count - i.inventory.quantity)


class ComponentInventory(Component):
    """ Component with inventory specific attributs """

    def __init__(
            self,
            footprint,
            value="",
            category="",
            source=None,
            fitted=True
    ):
        if source:
            self.price = source.get("price") or float('inf')
            self.distributors = distributors.DistributorItemGroup(
                source.get("distributors"))
            self.datasheet = source.get("datasheet") or ""
            self.quantity = source.get("stock") or 0
        else:
            self.price = float('inf')
            self.distributors = distributors.DistributorItemGroup()
            self.datasheet = ""
            self.quantity = 0
        self.fitted = fitted
        super().__init__(value, category, footprint, self.quantity)

    def str_own_parameters(self):
        """ string description containing only inventory specific fields"""
        if not self.fitted:
            return "not fitted"
        return self.distributors.__str__()

    def __str__(self):
        return Component.__str__(self) + self.str_own_parameters()

    def __repr__(self):
        return str(self)

    def to_object(self):
        """ Return the class fields to be saved """
        ret = {}
        if self.value:
            ret["value"] = self.value
        if self.footprint:
            ret["footprint"] = self.footprint
        if self.price and self.price != float('inf'):
            ret["price"] = self.price
        if self.distributors.to_object():
            ret["distributors"] = self.distributors.to_object()
        if self.datasheet:
            ret["datasheet"] = self.datasheet
        ret["stock"] = self.quantity or 0
        return ret


class Inventory:
    """ Inventory of components """

    @classmethod
    def to_list(cls, data, ret):
        """ Construct a list of compoents from the internal structure """
        if isinstance(data, ComponentInventory):
            ret.append(data)
        if isinstance(data, list):
            for i in data:
                cls.to_list(i, ret)
        if isinstance(data, dict):
            for i in data.values():
                cls.to_list(i, ret)

    @classmethod
    def to_object_rec(cls, data):
        """ Return the class fields to be saved using fields to_object"""
        if isinstance(data, ComponentInventory):
            return data.to_object()
        if isinstance(data, list):
            return [cls.to_object_rec(i) for i in data]
        if isinstance(data, dict):
            return {k: cls.to_object_rec(v) for k, v in data.items()}

    def __init__(self, components):
        self.data = {}
        for group_name, category in components.items():
            if group_name == "not_fitted":
                self.data["not_fitted"] = [
                    ComponentInventory(component["footprint"], fitted=False)
                    for component in category]
            elif group_name == "by_footprint":
                self.data["by_footprint"] = {category_name: [
                    ComponentInventory(
                        component["footprint"],
                        source=component)
                    for component in components
                    ] for category_name, components in category.items()}
            else:
                self.data[group_name] = {footprint_name: [
                    ComponentInventory(
                        footprint_name,
                        component["value"],
                        group_name,
                        component
                    ) for component in footprint
                    ] for footprint_name, footprint in category.items()}
        self.as_list = []
        Inventory.to_list(self.data, self.as_list)
        Global.debug("Inventory", json.dumps(
            self.as_list,
            indent=4,
            default=json_serial,
            ensure_ascii=False
        ))
        self.distributors = distributors.Distributors()

    def find_all(self, component):
        """find all inventory components matching a given component"""
        ret = []
        # Not fitted
        ret.extend([x for x in self.data["not_fitted"] if (
            component.footprint == x.footprint)])
        # By footprint
        if component.category in self.data["by_footprint"]:
            cat = self.data["by_footprint"][component.category]
            ret.extend([x for x in cat if component.footprint == x.footprint])
        # Comparable
        if component.category in self.data:
            cat = self.data[component.category]
            if component.footprint in cat:
                ret.extend([x for x in cat[component.footprint] if
                            component.fits(x)])
        return ret

    def find_best(self, component):
        """ Find the best component from a list of fitting components """
        choice = self.find_all(component)
        if not choice:
            return None
        choice.sort(key=lambda x: x.price if x.price else float('Inf'))
        return choice[0]

    def update_distributors(self):
        """ Retrive price from all components in inventory and store it """
        self.distributors.update_distributors(self.as_list)

    def to_object(self):
        return Inventory.to_object_rec(self.data)


if __name__ == '__main__':

    import unittest
    import os
    import kicad_netlist_reader

    class TestComponent(unittest.TestCase):
        """ Component unit tests """

        def test_all(self):
            """ Component unit tests """
            with open(Global.inventory_filename) as inventory_file:
                inventory_load = json.load(inventory_file)
            ivt = Inventory(inventory_load)
            board_count = 10
            netlists = [
                "tools/tests/component/test_component.xml",
                "tools/tests/parser/test_parser.xml",
            ]
            for netlist in netlists:
                net = kicad_netlist_reader.netlist(
                    os.path.join(Global.dir_path, netlist))
                groups = net.groupComponents()
                bom = BOM(groups)
                bom.compute_all(ivt, board_count)

    unittest.main()
