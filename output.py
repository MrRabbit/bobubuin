#!/usr/bin/env python3

# bobubuin
# BOM, BUy, BUild and INventory helper
#
# Copyright (C) 2019 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact bobubuin at mrabb dot it

""" BOM and inventory components """


import csv
from distributors import Distributors


class Output:
    """ Writes the BOM to a file """

    def __init__(self, bom, board_count):
        dists = Distributors().distributors()
        self.values = []
        self.model = [
            "refs",
            "value",
            "category",
            "footprint",
            "quantity",
            "stock",
        ]
        self.totals = []
        for i in dists:
            self.model.append(i + "_sku")
            self.model.append(i + "_to_order")
            self.model.append(i + "_unit_price")
            self.model.append(i + "_total_price")
            self.totals.append(self.model.index(i + "_total_price"))

        for i in bom.components:
            print(str(i.value))
            value = [
                ", ".join(sorted(i.refs)),
                str(i.value),
                str(i.category),
                str(i.footprint),
                str(i.quantity),
                str(i.inventory.quantity if i.inventory else 0)
            ]
            ignore = False
            if i.inventory:
                if i.inventory.fitted:
                    for dist in dists:
                        if (i.inventory.distributors and
                                dist in i.inventory.distributors.dists):
                            dist_item = i.inventory.distributors.dists[dist]
                            value.append(dist_item.sku)
                            (qnty, unit_price) = dist_item.getPrice(
                                i.quantity * board_count -
                                i.inventory.quantity)
                            value.append(qnty)
                            value.append(unit_price)
                            value.append(round(qnty * unit_price, 4))
                        else:
                            value.extend(["-"] * 4)
                else:
                    ignore = True
            else:
                value.extend(["_"] * 4 * len(dists))
            if not ignore:
                self.values.append(value)

    def write_csv(self, filename, extra_info={}):
        """ Write a computed BOM into a CSV file """
        csv_output = open(filename, 'w')
        out = csv.writer(
            csv_output,
            lineterminator='\n',
            delimiter=';',
            quotechar='\"',
            quoting=csv.QUOTE_NONNUMERIC)
        start = 1
        for k, v in extra_info.items():
            out.writerow([k, v])
            start += 1
        out.writerow(self.model)
        end = start
        for i in self.values:
            out.writerow(i)
            end += 1
        # add total
        total = []
        for i, _ in enumerate(self.model):
            if i in self.totals:
                col = chr(65 + i)
                total.append("=SUM(%s%d:%s%d)" % (col, start + 1, col, end))
            else:
                total.append("")
        out.writerow(total)
